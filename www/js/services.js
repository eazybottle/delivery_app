angular.module('eazydelivery')

	.factory('Popup', function ($ionicPopup) {
		return {
			showAlert: function (sub) {
				var alertPopup = $ionicPopup.alert({
					title: sub,
					cssClass: 'button-later' //need to test this
				});
				return alertPopup;
			}
		};
	})

	.factory('Login', function ($http, SERVER) {
		return {
			signin: function(usr){
				var req = {
                    method: 'POST',
                    url: SERVER.URL +'/EazyBottle/app/user/login',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
                    },
                    params: usr
                };
                return $http(req);
			},
		};

	})

	.factory('Notification', function ($http, SERVER) {
		return {
			setToken: function(token){
				var req = {
                    method: 'POST',
                    url: SERVER.URL +'/EazyBottle/app/rest/staff/register_token',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
                    },
                    params: token
                };
                return $http(req);
			},
		};

	})

	.service('DeliveriesService', function ($http, SERVER) {
        return {
            getDeliveries: function () {
                var url= SERVER.URL +'/EazyBottle/app/rest/delivery/upcoming';
                return $http.get(url);
            },
            updateDelivery: function (data) {
                var req = {
                    method: 'POST',
                    url: SERVER.URL +'/EazyBottle/app/rest/delivery/update-status',
                    params: data
                };
                return $http(req);
            },
			getCompleted: function () {
				var url= SERVER.URL +'/EazyBottle/app/rest/delivery/completed';
                return $http.get(url);
			},
			getFuture: function () {
				var url= SERVER.URL +'/EazyBottle/app/rest/delivery/future';
                return $http.get(url);
			},
			getMonthly: function(){
				var url= SERVER.URL +'/EazyBottle/app/rest/staff/deliveries/montly';
                return $http.get(url);	
			}
        };
    })

	.service('UserService', function ($http, SERVER) {		
		
		this.getInfo = function () {
			return $http.get(SERVER.URL + '/EazyBottle/app/rest/staff/info', { cache: false });//{timeout: 30000, cache: false}
			// .then(function (res) {
			// 	deferred.resolve(res.data);
			// 	return deferred.promise;
			// }, function (res) {
			// 	deferred.reject(res);
			// 	return deferred.promise;
			// });
		};
		
		this.totalBottles = function (arr) {
			var result = [];
			arr.forEach(function (obj) {
				if (obj.refundStatus == 'NONE') { //check if the bottles are requested for refund
					result.push(obj.paid + obj.free);
				}
			});
			var bottles = result.reduce(function (a, b) {
				return a + b;
			}, 0);
			if (bottles.toString().length == 1) {
				return '0' + bottles
			} else {
				return bottles;
			}
		};
	})

	

	.factory('Auth', function () {
		return {
			token: function (response) {
				window.localStorage.access_token = response.access_token;
				window.localStorage.refresh_token = response.refresh_token;
				window.localStorage.expires_in = response.expires_in;
				// var expireDate = new Date(new Date().getTime() + (1000 * response.expires_in));
			},
			getAccessToken: function () {
				return window.localStorage.access_token;
			},
			getRefreshToken: function () {
				return window.localStorage.refresh_token;
			},
			clear: function () {
				window.localStorage.clear();
			},
			newAccessToken: function () {
				var self = this;
				var data = {
					grant_type: 'refresh_token',
					client_id: 'my-trusted-client',
					refresh_token: self.getRefreshToken()
				};
				return data;
			},
			isLoggedIn: function () {
				if (window.localStorage.refresh_token) {
					return true;
				} else {
					return false;
				}
			},
		};
	})

	.factory('AuthInterceptor', function ($q, $injector, Auth, SERVER) {
		var inFlightAuthRequest = null;
		var count = 1;
		return {
			request: function (config) {
				if (Auth.getAccessToken()) {
					config.headers.Authorization = 'Bearer ' + Auth.getAccessToken();
				}
				return config;
			},
			responseError: function (response) {
				console.log("interceptor responseError", response);
				switch (response.status) {
					case 401:
						// if(response.data.error !== 'unauthorized' && response.data.error_description !== null){
						if (response.data.error === 'invalid_token' && (response.data.error_description.split(':')[0] === 'Access token expired' || response.data.error_description.split(':')[0] === 'Invalid access token')) {
							console.log("inside the Access Token expired condition");
							var deferred = $q.defer();
							if (!inFlightAuthRequest) {
								console.log("inside the inFlightAuthRequest condition requesting tokens");
								inflightAuthRequest = $injector.get("$http").post(SERVER.URL + '/EazyBottle/app/oauth/token/?grant_type=refresh_token&client_id=my-trusted-client&refresh_token=' + Auth.getRefreshToken());
							}
							// var inflightAuthRequest = $injector.get("$http").post('/api/oauth/', { params: Auth.newAccessToken() });
							inflightAuthRequest.then(function (r) {
								inflightAuthRequest = null;
								console.log("inside the inFlightAuthRequest.then api call");
								if (typeof r !== 'undefined' && r.data.access_token && r.data.refresh_token) {
									Auth.token(r.data);
									console.log('requested auth tokens ', r);
									console.log('failed apis config', response);
									$injector.get("$http")(response.config).then(function (resp) {
										deferred.resolve(resp);
									}, function (resp) {
										deferred.reject();
									});
								} else {
									deferred.reject();
								}
							}, function (response) {
								inflightAuthRequest = null;
								deferred.reject();
								Auth.clear();
								$injector.get("$state").go('login');
								// return;
							});
							return deferred.promise;
						} else {
							inflightAuthRequest = null;
							Auth.clear();
							$injector.get("$state").go('login');
							// return;
						}
						break;
					case 500:
						// retry after the 500 error
						console.log("inside the 500 error case");
						var deferred = $q.defer();
						// var count = 1;
						console.log("inside the 500 error case count value " + count);
						if (count < 4) {
							count++;
							$injector.get("$http")(response.config).then(function (resp) {
								deferred.resolve(resp);
							}, function (resp) {
								deferred.reject();
							});
							return deferred.promise;
						} else {
							// window.plugins.toast.showShortBottom(
							// 	"Internet Connection Problem", function (a) { }, function (b) { }
							// );
							console.log('500 case reached limit condition');
						}
						break;
					case 404:
						window.plugins.toast.showShortBottom(
							"Server Page not found", function (a) { }, function (b) { }
						);
						break;
					case 400:
						if (response.data.error_description === 'Bad credentials') {
							window.plugins.toast.showShortBottom(
								"Invalid Credentials", function (a) { }, function (b) { }
							);
						} else {
							window.plugins.toast.showShortBottom(
								"Bad Request", function (a) { }, function (b) { }
							);
						}
						break;
					case -1:
						window.plugins.toast.showShortBottom(
							"Internet Connection Problem", function (a) { }, function (b) { }
						);
						break;
					default:
						Auth.clear();
						$injector.get("$state").go('login');
						break;
				}
				return response || $q.when(response);
			}
		};
	})

	.config(function ($httpProvider) {
		$httpProvider.interceptors.push('AuthInterceptor');
	})

