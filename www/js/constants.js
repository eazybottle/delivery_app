angular.module('eazydelivery')

    .constant('SERVER', {
        // URL: 'http://www.eazybottle.jvmhost.net'
        URL: 'https://eazybottle.com'
    })

    .constant('PRODUCT', {
        ADD_A_BOTTLE: 1
    })

    .constant('$ionicLoadingConfig', {
        template: '<ion-spinner icon="android"></ion-spinner>',
        animation: 'fade-in',
        showBackdrop: true,
        showDelay: 0
    })