angular.module('eazydelivery', ['ionic', 'ngCordova', 'ionic.cloud', 'ionic.ion.imageCacheFactory', 'ionic.closePopup'])

	.run(function ($ionicPlatform, $rootScope, $cordovaNetwork, $window, $ionicHistory, $state) {
		$ionicPlatform.ready(function () {
			//$window.localStorage.clear();
			$ionicHistory.clearCache();
			$ionicHistory.clearHistory();
			// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
			// for form inputs)
			if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
				cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
				cordova.plugins.Keyboard.disableScroll(true);

			}
			if (window.StatusBar) {
				// org.apache.cordova.statusbar required
				StatusBar.styleDefault();
			}
			//native click sound using plugin
			var clickyClasses = ['sound-click', 'button'];
			nativeclick.watch(clickyClasses);

			// Check for network connection
			var type = $cordovaNetwork.getNetwork();
			var isOnline = $cordovaNetwork.isOnline();
			var isOffline = $cordovaNetwork.isOffline();
			// listen for Online event
			$rootScope.$on('$cordovaNetwork:online', function (event, networkState) {
				var onlineState = networkState;
				console.log('ONLINE STATE ', networkState);
				$rootScope.showCard = false;
			});

			// listen for Offline event
			$rootScope.$on('$cordovaNetwork:offline', function (event, networkState) {
				var offlineState = networkState;
				console.log('OFFLINE STATE ', networkState);
				$rootScope.showCard = true;
			});
			$ionicPlatform.registerBackButtonAction(function () {
				if ($state.current.name === "tab.dash") {
					if ($rootScope.backButtonPressedOnceToExit) {
						ionic.Platform.exitApp();
					}
					else {
						$rootScope.backButtonPressedOnceToExit = true;
						window.plugins.toast.showShortBottom(
							"Double tap to exit", function (a) { }, function (b) { }
						);
						setTimeout(function () {
							$rootScope.backButtonPressedOnceToExit = false;
						}, 2000);
					}
				} else {
					$ionicHistory.goBack();
				}
			}, 100);

			//polyfills for 4.4.4 versions and earlier
			if (!Array.prototype.find) {
				Object.defineProperty(Array.prototype, 'find', {
					value: function (predicate) {
						'use strict';
						if (this == null) {
							throw new TypeError('Array.prototype.find called on null or undefined');
						}
						if (typeof predicate !== 'function') {
							throw new TypeError('predicate must be a function');
						}
						var list = Object(this);
						var length = list.length >>> 0;
						var thisArg = arguments[1];
						var value;

						for (var i = 0; i < length; i++) {
							value = list[i];
							if (predicate.call(thisArg, value, i, list)) {
								return value;
							}
						}
						return undefined;
					}
				});
			} //for Android versions 4.4.4 earlier 
			if (!Array.prototype.findIndex) {
				Object.defineProperty(Array.prototype, 'findIndex', {
					value: function (predicate) {
						'use strict';
						if (this == null) {
							throw new TypeError('Array.prototype.findIndex called on null or undefined');
						}
						if (typeof predicate !== 'function') {
							throw new TypeError('predicate must be a function');
						}
						var list = Object(this);
						var length = list.length >>> 0;
						var thisArg = arguments[1];
						var value;

						for (var i = 0; i < length; i++) {
							value = list[i];
							if (predicate.call(thisArg, value, i, list)) {
								return i;
							}
						}
						return -1;
					},
					enumerable: false,
					configurable: false,
					writable: false
				});
			} //for Android versions 4.4.4 earlier
			if (!Array.prototype.filter) {
				Array.prototype.filter = function (fun/*, thisArg*/) {
					'use strict';

					if (this === void 0 || this === null) {
						throw new TypeError();
					}

					var t = Object(this);
					var len = t.length >>> 0;
					if (typeof fun !== 'function') {
						throw new TypeError();
					}

					var res = [];
					var thisArg = arguments.length >= 2 ? arguments[1] : void 0;
					for (var i = 0; i < len; i++) {
						if (i in t) {
							var val = t[i];

							// NOTE: Technically this should Object.defineProperty at
							//       the next index, as push can be affected by
							//       properties on Object.prototype and Array.prototype.
							//       But that method's new, and collisions should be
							//       rare, so use the more-compatible alternative.
							if (fun.call(thisArg, val, i, t)) {
								res.push(val);
							}
						}
					}

					return res;
				};
			}//4.4.4 versions earlier
		});

	})

	.config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider, $ionicCloudProvider) {

		$ionicCloudProvider.init({
			"core": {
				"app_id": ""
			}
		});

		$ionicConfigProvider.tabs.position('bottom');

		$ionicConfigProvider.scrolling.jsScrolling(false); //for Native Scroll laf

		$ionicConfigProvider.navBar.alignTitle('center');

		$stateProvider

			.state('login', {
				url: '/login/',
				templateUrl: 'templates/login.html',
				controller: 'LoginCtrl'
			})

			// setup an abstract state for the tabs directive
			.state('tab', {
				url: '/tab',
				abstract: true,
				templateUrl: 'templates/tabs.html'
			})

			// Each tab has its own nav history stack:

			.state('tab.dash', {
				url: '/dash/',
				views: {
					'tab-dash': {
						templateUrl: 'templates/tab-dash.html',
						controller: 'DashCtrl',
					}
				},
				onEnter: function ($state, Auth) {
					if (!Auth.isLoggedIn()) {
						$state.go('login');
					} else {
						console.log('logged in already!')
					}
				}
			})

			.state('tab.complete', {
				url: '/complete',
				views: {
					'tab-complete': {
						templateUrl: 'templates/tab-complete.html',
						controller: 'CompletedDeliveryCtrl'
					}
				}
			})

			.state('tab.upcoming', {
				url: '/upcoming',
				views: {
					'tab-upcoming': {
						templateUrl: 'templates/tab-upcoming.html',
						controller: 'UpcomingCtrl'
					}
				}
			});

		$urlRouterProvider.otherwise('/tab/dash/');

	});
