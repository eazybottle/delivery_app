angular.module('eazydelivery')

	.controller('LoginCtrl', function ($scope, $state, $ionicLoading, Login, Auth, UserService) {

		$scope.login = {
			phoneNumber: '',
			terms: true,
			password: ''
		};
		$scope.loginUser = function () {
			console.log('login data is ', $scope.login);
			if (/^[0-9]{10}$/.test($scope.login.phoneNumber) && $scope.login.terms && $scope.login.password) {
				$ionicLoading.show({
					template: '<ion-spinner icon="ripple" class="spinner-calm"></ion-spinner><p>Loggin In</p>',
					animation: 'fade-in',
					showBackdrop: true,
					showDelay: 0
				});
				$scope.errMsg = '';//setting the error message back to empty
				$scope.user = {
					//grant_type: 'password',
					//client_id: 'my-trusted-client',
					phone: $scope.login.phoneNumber,
					otp: $scope.login.password
				};

				Login.signin($scope.user).then(loginSuccess, loginFailure);
				function loginSuccess(result) {
					$ionicLoading.hide();
					console.log('success response after login', result);
					if (result.data.success) {
						Auth.token(result.data.response);
						//user api call
						UserService.getInfo()
							.then(function (result) {
								if (result.data.success) {
									console.log('User promise is resolved', result);
								} else {
									console.log('User promise something went wrong');
								}
							}, function (error) {
								console.log('User promise is rejected', error);
							});
						$state.go('tab.dash');
					} else {
						window.plugins.toast.showShortBottom(
							result.data.errorMsg, function (a) { }, function (b) { }
						);
					}
				};
				function loginFailure(error) {
					console.log('loginFailure response is ', error);
				};

			} else if (!$scope.login.terms) {
				$scope.errMsg = 'Please agree to the terms and conditions to proceed';
			} else if (!$scope.login.password) {
				$scope.errMsg = 'Password can not be empty';
			} else {
				$scope.errMsg = 'Please enter a valid 10 - digit mobile number';
			}
		};
	})

	.controller('DashCtrl', function ($rootScope, $scope, $state, $ionicPopup, $ionicLoading, $cordovaLaunchNavigator, $ionicPlatform, $ionicDeploy, $ionicModal, DeliveriesService, UserService, SERVER, IonicClosePopupService) {

		$scope.$on('$ionicView.enter', function (e) {
			$scope.doRefresh();
			//$scope.checkForUpdate();
		});
		$scope.doRefresh = function () {
			DeliveriesService.getDeliveries().then(deliverySuccess, deliveryError);
			function deliverySuccess(res) {
				console.log('deliveries list ', res);
				if (res.data.success) {
					var arr = res.data.response;
					arr.forEach(function (obj) {
						var lastModi = obj.deliveryTime;
						var date = new Date(+lastModi);
						var month = date.getMonth() + 1;
						var hour = date.getHours();
						if (obj.deliveryType === "SCHEDULE") {
							hour += 1;
						}
						var min = date.getMinutes();
						var sec = date.getSeconds();
						var bottleCount = UserService.totalBottles(obj.customer.bottles);
						obj.customer.walletBottles = bottleCount;
						obj.deliveryWithin = hour + ':' + min ;
						// obj.deliveryWithin = hour + ':' + min + ':' + sec;
					});
					$scope.deliveries = arr;
					$rootScope.deliveriesLength = arr.length;
				} else {
					console.log('deliveries list error ', res);
					window.plugins.toast.showShortBottom(
						res.data.errorMsg, function (a) { }, function (b) { }
					);
				}
				//Stop the ion-refresher from spinning
      			$scope.$broadcast('scroll.refreshComplete');
			};
			function deliveryError(res) {
				console.log('deliveries list error ', res);
			};
		};

		$scope.launchNavigator = function (latitude, longitude) {
			var destination = [latitude, longitude];
			$cordovaLaunchNavigator.navigate(destination, null).then(function () {
				console.log("Navigator launched");
			}, function (err) {
				console.error(err);
			});
		};

		$scope.update = function (id, num, ret) {
			$scope.delivery = {
				selected: '',
				noOfBottles: num,
				returnBottles: ret,
			};
			// dynamicAlert.showAlert('Take Action', '<ion-checkbox ng-model="delivery.selected" ng-true-value="delivered" ng-false-value="false">Delivered</ion-checkbox> <ion-checkbox ng-model="delivery.selected" ng-true-value="notDelivered" ng-false-value="false">Not Delivered</ion-checkbox>');
			var myPopup = $ionicPopup.show({
				templateUrl: 'templates/popup-takeAction.html',
				title: 'Take Action',
				scope: $scope,
				buttons: [
					{
						text: '<b>Update</b>',
						type: 'button-get-now',
						onTap: function (e) {
							//check for selected and num of bottles
							console.log('the delivery status for ' + id + ' selected delivery ' + $scope.delivery.selected + ' no of bottles delivered ' + $scope.delivery.noOfBottles +' no. of bottles returned'+ $scope.delivery.returnBottles);
							// return $scope.delivery;
							//if ((typeof $scope.delivery.selected == 'boolean' || typeof $scope.delivery.selected == 'boolean') && $scope.delivery.noOfBottles && $scope.delivery.returnBottles) {
							if ( typeof $scope.delivery.selected == 'boolean' && $scope.delivery.noOfBottles ) {
								$ionicLoading.show({
									template: '<ion-spinner icon="ripple" class="spinner-calm"></ion-spinner><p>Loading...</p>',
									animation: 'fade-in',
									showBackdrop: true,
									showDelay: 0
								});
								DeliveriesService.updateDelivery({ deliveryId: id, noOfBottle: $scope.delivery.noOfBottles, delivered: $scope.delivery.selected, noOfReturnCans: $scope.delivery.returnBottles }).then(updateSuccess, updateError);
								function updateSuccess(res) {
									$ionicLoading.hide();
									if (res.data.success) {
										console.log('updated delivery list after update success ', res);
										var arr = res.data.response;
										arr.forEach(function (obj) {
											var lastModi = obj.deliveryTime;
											var date = new Date(+lastModi);
											var month = date.getMonth() + 1;
											var hour = date.getHours();
											if (obj.deliveryType === "SCHEDULE") {
												hour += 1;
											}
											var min = date.getMinutes();
											var sec = date.getSeconds();
											var bottleCount = UserService.totalBottles(obj.customer.bottles);
											obj.customer.walletBottles = bottleCount;
											obj.deliveryWithin = hour + ':' + min ;
											// obj.deliveryWithin = hour + ':' + min + ':' + sec;
										});
										$scope.deliveries = arr;
										$rootScope.deliveriesLength = arr.length;
									} else {
										console.log('updated delivery list after update success false ', res);
									}
								};
								function updateError(res) {
									$ionicLoading.hide();
									console.log('update error ', res);
								};
							} else {
								if($scope.noOfBottles == undefined){
									console.log('out of limit');
									window.plugins.toast.showShortBottom(
										"Enter values between 0 and 999", function (a) { }, function (b) { }
									);
								}
								else if($scope.noOfReturnCans === undefined){
									console.log('out of limit');	
									window.plugins.toast.showShortBottom(
										"Enter values between 0 and 999", function (a) { }, function (b) { }
									);		
								}						
								else{
									window.plugins.toast.showShortBottom(
										"Please choose the options", function (a) { }, function (b) { }
									);
								}
							}
						}
					},
					{
						text: '<b>Cancel</b>',
						onTap: function(e) {
							myPopup.close();
						}
					}
				]
			});
			myPopup.then(function (res) {
				console.log('the delivery upated list ', $scope.deliveries);
			});
			IonicClosePopupService.register(myPopup);
		};

		//profile modal page
		$ionicModal.fromTemplateUrl('templates/modal-profile.html', {
			scope: $scope,
			animation: 'slide-in-right'
		}).then(function(modal){
			$scope.profileModal = modal;
		});
		//user api call
		UserService.getInfo()
			.then(function (result) {
				if (result.data.success) {
					console.log('User promise is resolved', result);
					$scope.user = {
						name: result.data.response.firstname,
						phone: result.data.response.phone
					};
				} else {
					console.log('User promise something went wrong');
				}
			}, function (error) {
				console.log('User promise is rejected', error);
			});

		$scope.showProfile = function(){
			$scope.profileModal.show();
		}

		$scope.closeProfile = function(){
			$scope.profileModal.hide();			
		}
		//logout function
		$scope.logout = function () {
			var logOutPopUp = $ionicPopup.show({
				title: 'Do you want to log out?',
				scope: $scope,
				buttons: [{
					text: 'Nope',
					type: 'button-later',
					onTap: function (e) {
						logOutPopUp.close();
					}
				},
				{
					text: '<b>logout</b>',
					type: 'button-get-now',
					onTap: function (e) {
						window.localStorage.clear();
						ionic.Platform.exitApp();
					}
				}
				]
			});
		};
		$scope.showSearch = false;
     	$scope.searched = false;
     	$scope.searchItem = {};	
	  	$scope.search = function(){
	   	 	console.log('search');
	   	 	$scope.searchItem = {};
	   	 	$scope.searched = false;
	   	 	$scope.showSearch = !$scope.showSearch;
	   	};
	})

	.controller('CompletedDeliveryCtrl', function ($rootScope, $scope, $ionicLoading, DeliveriesService, UserService, SERVER) {
		$scope.$on('$ionicView.enter', function (e) {
			$scope.doRefresh();
			//$scope.checkForUpdate();
		});
		$scope.doRefresh = function () {
			DeliveriesService.getCompleted().then(deliverySuccess, deliveryError);
			function deliverySuccess(res) {
				console.log('completed deliveries list ', res);
				if (res.data.success) {
					var arr = res.data.response;
					var copyOfArr = [];
					copyOfArr = copyOfArr.concat(arr);
					var cancelledDeliveries = [];
					$rootScope.collectedAmount = 0;
					arr.forEach(function (obj) {
						var lastModi = obj.deliveredTime;
						var date = new Date(+lastModi);
						var month = date.getMonth() + 1;
						var hour = date.getHours();
						// if (obj.deliveryType === "SCHEDULE") {
						// 	hour += 1;
						// }
						var min = date.getMinutes();
						var sec = date.getSeconds();
						var bottleCount = UserService.totalBottles(obj.customer.bottles);
						obj.customer.walletBottles = bottleCount;
						obj.deliveredTime = hour + ':' + min + ':' + sec;

						console.log(obj.status);
						if(obj.status === 'CANCELLED'){
							cancelledDeliveries.push(obj);
							copyOfArr.splice(copyOfArr.indexOf(obj),1);
							console.log('removed');
						}
						else{
							//if(obj.paymentType === 'COD')
							$rootScope.collectedAmount += obj.amount;
						}
					});
					$scope.deliveries = copyOfArr;
					$scope.cancelled = cancelledDeliveries;
					$rootScope.allDeliveries[1] = arr;
					console.log('delivered', copyOfArr);
					console.log('cancelled', cancelledDeliveries);
					$rootScope.completedLength = copyOfArr.length;
					$rootScope.cancelledLength = cancelledDeliveries.length;
				} else {
					console.log('completed deliveries list error ', res);
					window.plugins.toast.showShortBottom(
						res.data.errorMsg, function (a) { }, function (b) { }
					);
				}
			};
			function deliveryError(res) {
				console.log('deliveries list error ', res);
			};
		};

		$scope.showDeliveredCard = true;
		$scope.deliveredColor = 'color:#7fcdff';
		$scope.cancelledColor = 'color:#000000';

		$scope.showCancelled = function () {
			$scope.showDeliveredCard = false;
			$scope.showCancelledCard = true;
			$scope.cancelledColor = 'color:#7fcdff';
			$scope.deliveredColor = 'color:#000000';
		};
		$scope.showDelivered = function () {
			$scope.showCancelledCard = false;
			$scope.showDeliveredCard = true;
			// add style to the text highlights
			$scope.deliveredColor = 'color:#7fcdff';
			$scope.cancelledColor = 'color:#000000'
		};
	})

	.controller('UpcomingCtrl', function ($rootScope, $scope, $ionicLoading, DeliveriesService, UserService, SERVER) {
		$scope.$on('$ionicView.enter', function (e) {
			$scope.doRefresh();
			//$scope.checkForUpdate();
		});
		$scope.doRefresh = function () {
			DeliveriesService.getFuture().then(deliverySuccess, deliveryError);
			function deliverySuccess(res) {
				console.log('future deliveries list ', res);
				if (res.data.success) {
					var arr = res.data.response;
					arr.forEach(function (obj) {
						var lastModi = obj.deliveryTime;
						var date = new Date(+lastModi);
						var month = date.getMonth() + 1;
						var hour = date.getHours();
						if (obj.deliveryType === "SCHEDULE") {
							hour += 1;
						}
						var min = date.getMinutes();
						var sec = date.getSeconds();
						var bottleCount = UserService.totalBottles(obj.customer.bottles);
						obj.customer.walletBottles = bottleCount;
						obj.deliveryWithin = hour + ':' + min + ':' + sec;
					});
					$scope.deliveries = arr;
					$rootScope.allDeliveries[2] = arr;
					$rootScope.upcomingLength = arr.length;
				} else {
					console.log('future deliveries list error ', res);
					window.plugins.toast.showShortBottom(
						res.data.errorMsg, function (a) { }, function (b) { }
					);
				}
			};
			function deliveryError(res) {
				console.log('deliveries list error ', res);
			};
		};
	})

	.controller('TabsCtrl', function($scope, $rootScope, DeliveriesService, Notification){

		var FCMready = false;
		var token = {};
		var detectFCMPlugin = setInterval(function(){
			if(FCMPlugin){
				console.log('entered');
				FCMready = true;
				FCMPlugin.onTokenRefresh(function(refreshToken){
				   	console.log('refresh token',refreshToken);
				   	token.fcm_token = refreshToken;
				   	Notification.setToken(token).then(function(res){
				   		console.log('notification token sent', res);	
				   	})
				});
				if(!token.fcm_token){
					FCMPlugin.getToken(function(gotToken){
					    console.log('got token',gotToken);
					    token.fcm_token = gotToken;
					    Notification.setToken(token).then(function(res){
					   		console.log('notification token sent', res);	
					   	})
					});
				}

				//FCMPlugin.onNotification( onNotificationCallback(data), successCallback(msg), errorCallback(err) )
				//Here you define your application behaviour based on the notification data.
				FCMPlugin.onNotification(function(data){
					if(data.wasTapped){
					//Notification was received on device tray and tapped by the user.
						//alert( JSON.stringify(data) );
					}else{
					//Notification was received in foreground. Maybe the user needs to be notified.
						//alert( JSON.stringify(data) );
					}
				});
				clearInterval(detectFCMPlugin);
			}
		},1000)

		

		$rootScope.collectedAmount = 0;
		$rootScope.allDeliveries = [];
		DeliveriesService.getDeliveries().then(function(res){
			$rootScope.deliveriesLength = res.data.response.length;
			console.log('current deliveries',$rootScope.deliveriesLength);
			$rootScope.allDeliveries.push(res.data.response);
		});

		DeliveriesService.getCompleted().then(function(res){
			var arr = res.data.response;
			var cancelledDeliveries = [];
			var copyOfArr = [];
			copyOfArr = copyOfArr.concat(arr);
			arr.forEach(function (obj) {
				if(obj.status === 'CANCELLED'){
					copyOfArr.splice(copyOfArr.indexOf(obj),1);
					cancelledDeliveries.push(obj);	
					console.log('removed');
				}
				else{
					if(obj.paymentType === 'COD')
						$rootScope.collectedAmount += obj.amount;
				}
			});
			$rootScope.completedLength = copyOfArr.length;
			$rootScope.cancelledLength = cancelledDeliveries.length;
			$rootScope.allDeliveries.push(res.data.response);
			console.log('completed deliveries',$rootScope.completedLength);
		});

		DeliveriesService.getFuture().then(function(res){
			$rootScope.upcomingLength = res.data.response.length;
			console.log('upcoming deliveries',$rootScope.upcomingLength);
			$rootScope.allDeliveries.push(res.data.response);
			console.log('total deliveries',$rootScope.allDeliveries);
		});

		DeliveriesService.getMonthly().then(function(res){
			console.log('monthly data', res.data.response.count);
			$rootScope.completedMonth = res.data.response.count;
		})
	})